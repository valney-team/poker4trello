# Poker4Trello

Welcome to the Source.

## Setup de Desenvolvimento

Versionamos apenas a parte que vai para deploy, por tanto o setup de desenvolvimento pode variar para cada desenvolvedor, bastando respeitar os requisítos mínimos do framework AVANT e as configurações do servidor de Produção, para consistência no fluxo de deploy.

No entando, sugerimos o uso do docker.

### Ambiente de Produção

```
Domain: poker4trello.mariovalney.com
PHP Version: 7.1
MySQL Version: 5.7.18
```

### Docker Compose

```yml
web:
  build: .docker
  links:
    - mysql:mysql
  ports:
    - "80:80"
    - "443:443"
  environment:
    - P4T_DB_NAME=poker4trello
    - P4T_DB_USER=root
    - P4T_DB_PASS=root
    - P4T_DB_HOST=172.17.0.2
  volumes:
    - ./www:/var/www/html
    - ./config.php:/var/www/html/config.php

mysql:
  image: mysql:5.7
  environment:
    - MYSQL_ROOT_PASSWORD=root
  volumes:
    - "./.data/db:/var/lib/mysql"
```

### Dockerfile

```
FROM php:7.1-apache

RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo pdo_mysql
RUN a2enmod rewrite
RUN service apache2 restart
```

### Config File

```
<?php

/**
 * Config: the configuration file.
 *
 * @package Avant
 */

/**
 * You know what to do...
 */

/** Site Config **/
define('SITE_NAME', 'DEV Poker4Trello');
define('LANG', 'pt_BR');

/**
 * Database configs
 * To deactivate the use of Database, exclude these 4 lines or set DB_NAME as empty: define('DB_NAME', '');
 */

define('DB_NAME', getenv( 'P4T_DB_NAME' ));
define('DB_USER', getenv( 'P4T_DB_USER' ));
define('DB_PASS', getenv( 'P4T_DB_PASS' ));
define('DB_HOST', getenv( 'P4T_DB_HOST' ));

/** Theme config **/
define('THEME', 'default');

/** Debug config **/
define('DEBUG', true);

/** URL and PATH config **/
define('BASE_URL', 'http://dev.poker4trello.mariovalney.com/');
define('CONFIG_FILE', __FILE__);

/** Software constants **/
define('TRELLO_API_KEY', '8f1e3c5a0a3a1bc758f8a6f7c53f836a');
define('SCRIPT_VERSION', date('s'));

/********************************************************************
 *
 *  That's all. Stop editing :)
 *
 ********************************************************************/

/** The directories name **/
define('CORE_DIR', 'core');
define('THEMES_DIR', 'themes');
```

### Como usar o Docker

Após instalar o Docker e o Docker-compose, crie um diretório para o projeto. Dentro dele crie os diretórios `www` e `.docker`.

Clone o repositório dentro do primeiro e insira o `Dockerfile` no segundo. Após isso o `docker-compose.yml` deve ir na raiz do projeto (ao lado dos dois diretórios criados).

Crie também o arquivo de configuração `config.php` e adicione ao lado do `docker-compose.yml` (esse arquivo é usado apenas localmente - com o Docker).

Agora basta rodar o comando `docker-compose up` que a imagem será criada. Crie um host para o site.

### Problemas?

Se tiver problemas com permissão de pasta, não esqueça de adicionar o seu usuário ao grupo `www-data` e definir o dono do repositório como `usuario:www-data`.

Após isso, se não conseguir atualizar nada ou baixar temas/plugins, tente corrigir as permissões (esteja na raiz do WordPress):

```bash
sudo find . -type d -exec chmod 775 {} \; && sudo find . -type f -exec chmod 664 {} \;
```

## Deploy

Para fazer o deploy basta um `push` ou merge para o branch `production`.