<?php

/**
 * API Database
 *
 * @package Avant
 */

namespace Avant\Api;

class Database {
    const ENTITIES = [
        'polls' => 'Avant\Api\Entities\Poll',
        'votes' => 'Avant\Api\Entities\Vote',
    ];

    /**
     * Instance.
     *
     * Holds the plugin instance.
     */
    public static $instance = null;

    /**
     * Database
     *
     * Holds avdb.
     */
    public $db = null;

    /**
     * Trello
     *
     * Holds Avant\API\Trello.
     */
    public $trello = null;

    /**
     * API Manager
     *
     * Holds Avant\API\Manager.
     */
    public $manager = null;

    public function __clone() { error_log( 'Cannot use __clone' ); }

    public function __wakeup() { error_log( 'Cannot use __wakeup' ); }

    /**
     * Instance.
     *
     * Ensures only one instance of the plugin class is loaded or can be loaded.
     */
    public static function instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Constructor.
     *
     * Initializing stuff.
     */
    private function __construct() {
        global $avdb;

        $this->db = $avdb;
        $this->trello = new Trello();
    }

    public function get( $entity, $filters = [] ) {
        $this->check_valid_request( $entity );

        $classname = self::ENTITIES[ $entity ];

        $where = array();
        if ( ! empty( $filters ) ) {
            $columns = $classname::filters();
            foreach ( $filters as $column => $value ) {
                if ( ! in_array( $column, $columns ) ) continue;

                $where[ $column ] = $value;
            }
        }

        $results = $this->db->select( $this->get_table( $entity ), null, $where );

        return array_map(function( $row ) use ( $classname ) {
            return $classname::get_instance( $row );
        }, $results );
    }

    public function post( $entity, $object ) {
        $this->check_valid_request( $entity );
        return $object->save();
    }

    public function delete( $entity, $object ) {
        $this->check_valid_request( $entity );
        return $object->delete();
    }

    private function check_valid_request( $entity ) {
        if ( ! in_array( $entity, array_keys( self::ENTITIES ) ) ) {
            $this->manager->invalid_endpoint_error();
        }
    }

    private function get_table( $entity ) {
        return call_user_func( self::ENTITIES[ $entity ] . '::table' );
    }
}