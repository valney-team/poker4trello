<?php

/**
 * API Entity Poll
 *
 * @package Avant
 */

namespace Avant\Api\Entities;

class Poll {
    /**
     * ID
     *
     * @var int
     */
    public $ID;

    /**
     * Board ID
     *
     * @var string
     */
    public $boardId = '';

    /**
     * Users
     *
     * @var array
     */
    public $users = array();

    /**
     * Owner ID
     *
     * @var string
     */
    public $owner = '';

    /**
     * Current voted card
     *
     * @var string
     */
    public $voting = '';

    /**
     * CreatedAt
     *
     * @var string
     */
    public $createdAt;

    /**
     * Retrieve instance.
     *
     */
    public static function get_instance( $poll = false ) {
        global $avdb;

        if ( is_object( $poll ) ) {
            $poll = sanitize_poll( $poll );
            return new Poll( $poll );
        }

        if ( ! $poll ) return false;

        $data = $avdb->select( self::table(), null, [ 'ID' => $poll ] );
        if ( empty( $data ) || empty( $data[0] ) ) return false;

        $data = sanitize_poll( $data[0] );

        return new Poll( $data );
    }

    public function __construct( $object ) {
        foreach ( get_object_vars( $object ) as $key => $value ) {
            $this->$key = $value;
        }
    }

    public function save() {
        global $avdb;

        $fields = array(
            'boardId',
            'users',
            'owner',
            'voting',
        );

        $values = array(
            $this->boardId,
            json_encode( $this->users ),
            $this->owner,
            $this->voting,
        );

        if ( empty( $this->createdAt ) ) {
            $this->createdAt = date('Y-m-d H:i:s');
        }

        if ( ! empty( $this->ID ) ) {
            return $avdb->update( self::table(), $fields, $values, 'ID', $this->ID );
        }

        return $avdb->insert( self::table(), $fields, $values );
    }

    public function delete() {
        global $avdb;

        if ( empty( $this->ID ) ) return true;
        return $avdb->delete( self::table(), [ 'ID' => $this->ID ] );
    }

    public function sanitize() {
        return sanitize_poll( $this );
    }

    public function to_array() {
        return get_object_vars( $this );
    }

    public function to_json() {
        return json_encode( $this );
    }

    public function has_user( $userId ) {
        foreach ( $this->users as $user ) {
            if ( $user->id == $userId ) return true;
        }

        return false;
    }

    public function add_user( $userId ) {
        if ( $this->has_user( $userId ) ) return;

        $this->users[] = (object) array(
            'id' => (string) $userId
        );
    }

    public function remove_user( $userId ) {
        if ( ! $this->has_user( $userId ) ) return;

        $this->users = array_filter( $this->users, function( $user ) use ( $userId ) {
            return ( $user->id != $userId );
        });
    }

    public static function table() {
        return 'polls';
    }

    public static function filters() {
        return [ 'ID', 'boardId' ];
    }
}

function sanitize_poll( $data ) {
    if ( is_object( $data ) ) {
        $data->users = ( is_array( $data->users ) ) ? $data->users : json_decode( $data->users );
        return $data;
    }

    $data['users'] = ( is_array( $data['users'] ) ) ? $data['users'] : json_decode( $data['users'] );
    return $data;
}