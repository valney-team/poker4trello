<?php

/**
 * API Entity Vote
 *
 * @package Avant
 */

namespace Avant\Api\Entities;

class Vote {
    /**
     * ID
     *
     * @var int
     */
    public $ID;

    /**
     * Poll ID
     *
     * @var string
     */
    public $poll;

    /**
     * User
     *
     * @var array
     */
    public $user;

    /**
     * Vote
     *
     * @var string
     */
    public $vote;

    /**
     * Retrieve instance.
     *
     */
    public static function get_instance( $vote = false ) {
        global $avdb;

        if ( is_object( $vote ) ) {
            $vote = sanitize_vote( $vote );
            return new Vote( $vote );
        }

        if ( ! $vote ) return false;

        $data = $avdb->select( self::table(), null, [ 'ID' => $vote ] );
        if ( empty( $data ) || empty( $data[0] ) ) return false;

        $data = sanitize_vote( $data[0] );

        return new Vote( $data );
    }

    public function __construct( $object ) {
        foreach ( get_object_vars( $object ) as $key => $value ) {
            $this->$key = $value;
        }
    }

    public function save() {
        global $avdb;

        $fields = array(
            'poll',
            'user',
            'vote',
        );

        $values = array(
            $this->poll,
            $this->user,
            $this->vote,
        );

        if ( ! empty( $this->ID ) ) {
            return $avdb->update( self::table(), $fields, $values, 'ID', $this->ID );
        }

        return $avdb->insert( self::table(), $fields, $values );
    }

    public function delete() {
        global $avdb;

        if ( empty( $this->ID ) ) return true;
        return $avdb->delete( self::table(), [ 'ID' => $this->ID ] );
    }

    public function sanitize() {
        return sanitize_vote( $this );
    }

    public function to_array() {
        return get_object_vars( $this );
    }

    public function to_json() {
        return json_encode( $this );
    }

    public static function table() {
        return 'votes';
    }

    public static function filters() {
        return [ 'ID', 'poll', 'user' ];
    }
}

function sanitize_vote( $data ) {
    if ( is_object( $data ) ) {
        $data->poll = (string) $data->poll;
        $data->user = (string) $data->user;
        $data->vote = (string) $data->vote;

        return $data;
    }

    $data['poll'] = (string) $data['poll'];
    $data['user'] = (string) $data['user'];
    $data['vote'] = (string) $data['vote'];

    return $data;
}