<?php

/**
 * API Manager
 *
 * @package Avant
 */

namespace Avant\Api;

class Manager {
    const INVALID_ENDPOINT = '404';

    const INVALID_AUTH = '403';

    public $request = array();

    private $apifile = '';

    public function __construct( $params ) {
        $this->request = array_slice( $params, 2 );
        $params = array_slice( $params, 0, 2 );

        $apifile = ROOT . THEMES_DIR . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;
        $apifile .= 'api' . DIRECTORY_SEPARATOR . implode( $params, DIRECTORY_SEPARATOR ) . '.php';

        $this->apifile = $apifile;
    }

    public function is_valid() {
        return file_exists( $this->apifile );
    }

    public function run() {
        Database::instance( $this );
        require_once $this->apifile;
    }

    public function api_success( $data = array() ) {
        av_send_json( $this->get_success( [ 'data' => $data ] ) );
    }

    public function api_error( $data = array() ) {
        av_send_json( $this->get_error( [ 'data' => $data ] ) );
    }

    public function invalid_endpoint_error() {
        av_send_json( $this->get_error( [ 'error' => self::INVALID_ENDPOINT ] ) );
    }

    public function invalid_auth_error() {
        av_send_json( $this->get_error( [ 'error' => self::INVALID_AUTH ] ) );
    }

    private function get_error( $data = array() ) {
        return array_merge( $data, [ 'success' => false ] );
    }

    private function get_success( $data = array() ) {
        return array_merge( $data, [ 'success' => true ] );
    }
}