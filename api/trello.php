<?php

/**
 * API Trello
 *
 * @package Avant
 */

namespace Avant\Api;

class Trello {
    const TRELLO_API = 'https://api.trello.com/1/';

    private $token = null;

    public function is_authorized() {
        return ( ! empty( $this->get_token() ) );
    }

    public function get_token_info( $extra_requests = [] ) {
        $extra_requests = (array) $extra_requests;

        $urls = array_merge( array(
            '/members/me',
            '/members/me/boards',
        ), $extra_requests );

        $urls = implode( ',', $urls );

        $answer = $this->request( 'batch', [ 'urls' => $urls ] );

        $info = array(
            'userId'    => 0,
            'boards'    => [],
            'extra'     => array_fill( 0, count( $extra_requests ), '' ),
        );

        if ( empty( $answer ) || ! is_array( $answer ) ) return $info;

        if ( ! empty( $answer[0] ) && ! empty( $answer[0]->{'200'} ) ) {
            $info['userId'] = $answer[0]->{'200'}->id;
        }

        if ( ! empty( $answer[1] ) && ! empty( $answer[1]->{'200'} ) && is_array( $answer[1]->{'200'} ) ) {
            $info['boards'] = array_map( function( $board ) {
                return $board->shortLink;
            }, $answer[1]->{'200'} );
        }

        if ( count( $answer ) <= 2 ) return $info;

        $extra_answers = array_slice( $answer, 2 );
        foreach ( $extra_answers as $key => $response ) {
            if ( empty( $response->{'200'} ) ) continue;

            $info['extra'][ $key ] = $response->{'200'};
        }

        return $info;
    }

    public function create_voted_card_name( $card_id, $vote ) {
        $regex = "/^(\(\S*\)[ ]?)/m";

        $card = $this->request( '/cards/' . $card_id );

        if ( empty( $card->id ) ) return false;

        $new_value = '(' . trim( $vote ) . ') ';

        $name = $card->name ?? '';

        $matches = [];
        preg_match( $regex, $name, $matches);

        if ( ! empty( $matches ) && ! empty( $matches[1] ) ) {
            $name = preg_replace( $regex, $new_value, $name );
        } else {
            $name = $new_value . $name;
        }

        return $name ?? false;
    }

    private function request( $url, $params = [] ) {
        $url = self::TRELLO_API . $url;
        $url .= '?' . http_build_query( array_merge( $params, array(
            'token' => $this->token,
            'key'   => TRELLO_API_KEY,
        ) ) );

        $url = urldecode( $url );

        $curl = curl_init();

        curl_setopt( $curl, CURLOPT_URL, $url );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );

        $answer = curl_exec( $curl );
        curl_close( $curl );

        return json_decode( $answer );
    }

    private function get_token() {
        if ( ! $this->token ) {
            if ( ! empty( $_REQUEST['token'] ) ) {
                $this->token = $_REQUEST['token'];
                return $this->token;
            }

            $headers = getallheaders();
            foreach ( $headers as $header => $value ) {
                $headers[ mb_strtoupper( $header ) ] = $value;
            }

            $this->token = $headers['X-P4T-AUTHTOKEN'] ?? null;
        }

        return $this->token;
    }
}