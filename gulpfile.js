var gulp = require('gulp');

// Plugins
var add = require("gulp-add");
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var iife = require("gulp-iife");
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglifyEs = require('gulp-uglify-es').default;
var watch = require('gulp-watch');

// Directories
var theme_source_dir = 'themes/app/source/',
    theme_dist_dir = 'themes/app/dist/';

/**
 * TASK: styles
 *
 * Proccess LESS to MAIN CSS
 * Prepare for DIST all CSS needed
 */

var css_files = [
    'node_modules/materialize-css/dist/css/materialize.css',
    theme_dist_dir + 'css/less.css'
];

function less_to_css() {
    return gulp.src(theme_source_dir + 'less/styles.less')
        .pipe(less())
        .pipe(rename('less.css'))
        .pipe(gulp.dest(theme_dist_dir + 'css'));
}

function css_to_dist() {
    return gulp.src(css_files)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(theme_dist_dir + 'css'))
        .pipe(browserSync.stream({stream: true}));
}

gulp.task('styles', gulp.series(less_to_css, css_to_dist));

/**
 * TASK: scripts
 *
 * Proccess LESS to MAIN JS
 * Prepare for DIST all JS needed
 */

var js_files = [
    'node_modules/materialize-css/dist/js/materialize.js',
    'node_modules/knockout/build/output/knockout-latest.debug.js',
    'node_modules/knockout-postbox/build/knockout-postbox.js',
    'node_modules/sortablejs/Sortable.js',
    'node_modules/showdown/dist/showdown.js',
    theme_source_dir + 'js/*.js',
    '!' + theme_source_dir + 'js/ready.js',
    theme_dist_dir + 'js/knockout.js',
    theme_source_dir + 'js/ready.js',
];

function build_application() {
    var applyComponents = 'ko.applyBindings();';

    return gulp.src([
            theme_source_dir + 'js/knockout/application.js',
            theme_source_dir + 'js/knockout/components/*.js',
        ])
        .pipe(add('applycomponents.js', applyComponents))
        .pipe(concat('knockout.js'))
        .pipe(iife({
            prependSemicolon: false,
            params: ['window', 'document', '$'],
            args: ['window', 'document', 'jQuery']
        }))
        .pipe(gulp.dest(theme_dist_dir + 'js'));
}

function js_to_dist() {
    return gulp.src(js_files)
        .pipe(add('usestrict.js', '"use strict";', true))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(theme_dist_dir + 'js'))
        .pipe(browserSync.reload({stream: true}));
}

gulp.task('scripts', gulp.series(build_application, js_to_dist));

/**
 * TASK: watch
 *
 * Keep watching for changes in directories to automate tasks
 */

function watch_changes() {
    browserSync.init({
        open: false,
        port: 8080,
        host: 'dev.poker4trello.mariovalney.com',
        proxy: 'dev.poker4trello.mariovalney.com',
        notify: false,
    });

    gulp.watch(theme_source_dir + 'less/*/*.less', gulp.series('styles'));
    gulp.watch([
        theme_source_dir + 'js/*.js',
        theme_source_dir + 'js/*/*.js',
        theme_source_dir + 'js/*/*/*.js'
    ], gulp.series('scripts'));

    gulp.watch(['themes/app/*.php', 'themes/app/**/*.html']).on('change', browserSync.reload);
}

gulp.task('watch', watch_changes);

/**
 * TASK: default
 * Create all things to distribute and deploy
 */

function styles_to_deploy() {
    return gulp.src(theme_dist_dir + 'css/styles.css')
        .pipe(rename('styles.min.css'))
        .pipe(minifyCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(theme_dist_dir + 'css'));
}

function jquery_to_dist() {
    return gulp.src(['node_modules/jquery/dist/jquery.min.js'])
        .pipe(gulp.dest(theme_dist_dir + 'js'))
        .pipe(browserSync.reload({stream: true}));
}

function scripts_to_deploy() {
    return gulp.src(theme_dist_dir + 'js/scripts.js')
        .pipe(rename('scripts.min.js'))
        .pipe(uglifyEs())
        .pipe(gulp.dest(theme_dist_dir + 'js'));
}

gulp.task('default', gulp.parallel(
    gulp.series('styles', styles_to_deploy),
    gulp.series('scripts', jquery_to_dist, scripts_to_deploy)
) );