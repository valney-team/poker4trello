## Creating table polls

```
CREATE TABLE poker4trello.polls (
    ID BIGINT NOT NULL AUTO_INCREMENT,
    boardId varchar(100) NOT NULL,
    users LONGTEXT NOT NULL,
    CONSTRAINT polls_PK PRIMARY KEY (ID)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
COMMENT='List of active polls';
```

## Making boardId unique

```
ALTER TABLE poker4trello.polls ADD CONSTRAINT polls_UN UNIQUE KEY (boardId) ;
```

## Creating owner/created_at columns

```
ALTER TABLE poker4trello.polls ADD owner varchar(100) NOT NULL;
ALTER TABLE poker4trello.polls ADD created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
```

## Creating voting columns

```
ALTER TABLE poker4trello.polls ADD voting VARCHAR(100) DEFAULT '' AFTER owner;
```

## Creating table votes

```
CREATE TABLE poker4trello.votes (
    ID BIGINT NOT NULL AUTO_INCREMENT,
    poll BIGINT NOT NULL,
    user VARCHAR(100) NOT NULL,
    vote VARCHAR(5) NOT NULL,
    CONSTRAINT polls_PK PRIMARY KEY (ID)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
COMMENT='List of active votes';

```