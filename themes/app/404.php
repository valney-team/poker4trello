<?php include_header(); ?>

<section class="page-wrapper page-center-height center-align">
    <h1 class="page-title"><?php _e( 'Essa é a 404' ); ?></h1>
    <p><?php _e( 'Não achamos nada...' ); ?></p>
</section>

<?php include_footer();
