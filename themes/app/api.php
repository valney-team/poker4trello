<?php
    global $avant;

    $version = ( $avant['query_params'][0] ) ?? '';
    if ( empty( $version ) ) av_redirect();

    $api = Avant\Api\Database::instance();
    $api->manager = new Avant\Api\Manager( $avant['original_params'] );

    if ( ! $api->manager->is_valid() ) {
        $api->manager->invalid_endpoint_error();
    }

    if ( ! $api->trello->is_authorized() ) {
        $api->manager->invalid_auth_error();
    }

    $api->manager->run();
?>
