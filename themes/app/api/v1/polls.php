<?php

use Avant\Api\Entities\Poll;

$api = Avant\Api\Database::instance();

$request = $api->manager->request;

$filters = array();

/**
 * GET
 * User is retrieving polls
 *
 * ENDPOINT /polls
 * List all polls user joined (or filter using GET 'boards')
 *
 * ENDPOINT /polls/board/{ID}
 * Retrieve the poll of board
 */

if ( empty( $_POST ) ) {
    // Try ENDPOINT filters
    if ( ! empty( $request ) && ! empty( $request[0] ) ) {
        if ( $request[0] == 'board' && ! empty( $request[1] ) ) {
            $filters['boardId'] = [ $request[1] ];
        } else if ( is_numeric( $request[0] ) ) {
            $filters['ID'] = $request[0];
        } else {
            $api->manager->invalid_endpoint_error();
        }
    }

    // Try GET filtersis_authorized
    if ( empty( $filters ) && ! empty( $_GET ) ) {
        if ( ! empty( $_GET['boards'] ) ) {
            $filters['boardId'] = explode( ',', $_GET['boards'] );
        }
    }

    // Check user can get this boards info
    $token = $api->trello->get_token_info();
    $allowedBoards = $token['boards'] ?? [];

    if ( empty( $filters['boardId'] ) ) {
        $filters['boardId'] = $allowedBoards;
    } else {
        $filters['boardId'] = array_filter( $filters['boardId'], function( $boardId ) use ( $allowedBoards ) {
            return in_array( $boardId, $allowedBoards );
        } );
    }

    $currentUser = (string) $token['userId'];

    $result = $api->get( 'polls', $filters ) ?? [];
    $result = array_map(function( $poll ) use ( $currentUser ) {
        $poll->participating = $poll->has_user( $currentUser );

        return $poll;
    }, $result);

    $api->manager->api_success( $result );
}


/**
 * POST
 * User is retrieving polls
 *
 * ENDPOINT /polls/create
 * Create a poll to the board (using POST 'board')
 *
 * ENDPOINT /polls/join
 * Join the poll of the board (using POST 'board' and 'poll')
 */

if ( ! empty( $request ) && ! empty( $_POST['board'] ) && in_array( $request[0], [ 'create', 'join' ] ) ) {
    // Check user can get create room
    $token = $api->trello->get_token_info();
    $allowedBoards = $token['boards'] ?? [];
    $userId = $token['userId'] ?? 0;

    if ( ! in_array( $_POST['board'], $allowedBoards ) ) {
        $api->manager->invalid_auth_error();
    }

    if ( $request[0] == 'create' ) {
        $newPoll = (object) array(
            'boardId'   => $_POST['board'],
            'owner'     => (string) $userId
        );

        $newPoll = new Poll( $newPoll );
        $newPoll->add_user( $userId );

        $result = $api->post( 'polls', $newPoll ) ?? 0;
        if ( empty( $result ) ) {
            $api->manager->api_error();
        }

        $newPoll->ID = $result;
        $newPoll->participating = true;
        $api->manager->api_success( $newPoll );
    }

    if ( empty( $_POST['poll'] ) ) {
        $api->manager->invalid_endpoint_error();
    }

    $poll = Poll::get_instance( $_POST['poll'] );

    if ( empty( $poll ) || empty( $poll->boardId ) || $poll->boardId !== $_POST['board'] ) {
        $api->manager->invalid_endpoint_error();
    }

    $poll->add_user( $userId );

    $result = $api->post( 'polls', $poll ) ?? 0;

    if ( ! empty( $result ) ) {
        $poll->participating = true;
        $api->manager->api_success( $poll );
    }

    $api->manager->api_error();
}

/**
 * POST
 * User is leving polls
 *
 * ENDPOINT /polls/delete
 * Remove a poll from the board (using POST 'id')
 *
 * ENDPOINT /polls/leave
 * Leave the poll of the board (using POST 'id')
 */
if ( ! empty( $request ) && ! empty( $_POST['id'] ) && in_array( $request[0], [ 'delete', 'leave' ] ) ) {
    // Sanitize poll ID
    $poll = (int) $_POST['id'];
    $poll = $api->get( 'polls', [ 'ID' => $poll ] );
    $poll = $poll[0] ?? [];

    if ( empty( $poll->ID ) || empty( $poll->boardId ) ) {
        $api->manager->api_error();
    }

    // Check user can get delete room
    $token = $api->trello->get_token_info();
    $allowedBoards = $token['boards'] ?? [];
    $userId = $token['userId'] ?? 0;

    if ( ! in_array( $poll->boardId, $allowedBoards ) ) {
        $api->manager->invalid_auth_error();
    }

    if ( $request[0] == 'delete' ) {
        if ( empty( $poll->owner ) || $poll->owner != $userId ) {
            $api->manager->invalid_auth_error();
        }

        if ( ! empty( $poll->voting ) ) {
            $api->manager->api_error();
        }

        // Get vote
        $votes = $api->get( 'votes', [ 'poll' => $poll->ID ] );
        // Remove votes
        foreach ( $votes as $vote ) {
            $api->delete( 'votes', $vote );
        }

        // Remove poll
        if ( $poll->delete() ) {
            $api->manager->api_success();
        }

        $api->manager->api_error();
    }

    if ( ! empty( $poll->owner ) && $poll->owner == $userId ) {
        $api->manager->invalid_auth_error();
    }

    $poll->remove_user( $userId );

    // Get vote
    $votes = $api->get( 'votes', [ 'poll' => $poll->ID, 'user' => $userId ] );

    // Remove votes
    foreach ( $votes as $vote ) {
        $api->delete( 'votes', $vote );
    }

    // Saving poll
    if ( $poll->save() ) {
        $api->manager->api_success();
    }

    $api->manager->api_error();
}

$api->manager->invalid_endpoint_error();