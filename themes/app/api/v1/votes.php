<?php

use Avant\Api\Entities\Poll;
use Avant\Api\Entities\Vote;

$api = Avant\Api\Database::instance();

$request = $api->manager->request;

$filters = array();

/**
 * GET
 *
 * ENDPOINT /votes/poll
 * List all votes (using POST 'id')
 */
if ( empty( $_POST ) ) {
    if ( ! empty( $request ) && ! empty( $_GET['id'] ) && $request[0] == 'poll' ) {
        // Sanitize poll ID
        $poll = (int) $_GET['id'];
        $poll = $api->get( 'polls', [ 'ID' => $poll ] );
        $poll = $poll[0] ?? [];

        if ( empty( $poll->ID ) || empty( $poll->boardId ) ) {
            $api->manager->api_error();
        }

        // Check user can access board
        $token = $api->trello->get_token_info();
        $allowedBoards = $token['boards'] ?? [];
        $userId = $token['userId'] ?? 0;

        if ( empty( $userId ) || ! in_array( $poll->boardId, $allowedBoards ) ) {
            $api->manager->api_error();
        }

        $votes = array(
            'user'      => '',
            'others'    => [],
        );

        $current_votes = $api->get( 'votes', [ 'poll' => $poll->ID ] );
        foreach ( $current_votes as $vote ) {
            if ( $vote->user == $userId ) {
                $votes['user'] = $vote->vote;
                continue;
            }

            $votes['others'][] = $vote;
        }

        return $api->manager->api_success( $votes );
    }

    $api->manager->invalid_endpoint_error();
}

/**
 * POST
 *
 * ENDPOINT /votes/card
 * Owner start voting (using POST 'id')
 */

if ( ! empty( $request ) && ! empty( $_POST['id'] ) && $request[0] == 'card' ) {
    // Sanitize card ID
    $card_id = filter_var( $_POST['id'], FILTER_SANITIZE_STRING );

    // Check user can get create room
    $extra_request = urlencode( '/cards/' . $card_id . '?board=true&board_fields=shortLink&sticker_fields=all' );

    $token = $api->trello->get_token_info( $extra_request );
    $allowedBoards = $token['boards'] ?? [];
    $userId = $token['userId'] ?? 0;
    $card = $token['extra'][0] ?? 0;

    if ( empty( $card ) || empty( $card->id ) ) {
        $api->manager->api_error();
    }

    if ( empty( $card->board ) || empty( $card->board->shortLink ) || ! in_array( $card->board->shortLink, $allowedBoards ) ) {
        $api->manager->api_error();
    }

    $current_board = $card->board->shortLink;

    $current_poll = $api->get( 'polls', [ 'boardId' => $current_board ] );
    $current_poll = $current_poll[0] ?? false;

    if ( empty( $current_poll ) ) {
        $api->manager->api_error();
    }

    if ( ! empty( $current_poll->voting ) ) {
        $api->manager->api_error();
    }

    $current_poll->voting = $card_id;

    if ( $current_poll->save() ) {
        $api->manager->api_success( $card );
    }

    $api->manager->api_error();
}

/**
 * POST
 *
 * ENDPOINT /votes/add
 * Add a vote (using POST 'poll', 'card', 'vote')
 */

if ( ! empty( $request ) && $request[0] == 'add' && ! empty( $_POST['poll'] ) && ! empty( $_POST['card'] ) && ! empty( $_POST['vote'] ) ) {
    // Sanitize card ID
    $poll = filter_var( $_POST['poll'], FILTER_SANITIZE_STRING );
    $card = filter_var( $_POST['card'], FILTER_SANITIZE_STRING );
    $new_vote = filter_var( $_POST['vote'], FILTER_SANITIZE_STRING );

    // Get the board of poll
    $poll = $api->get( 'polls', [ 'ID' => $poll ] );
    $poll = $poll[0] ?? [];

    if ( empty( $poll->ID ) ) {
        $api->manager->api_error();
    }

    // Check we are voting
    if ( empty( $poll->voting ) || $poll->voting != $card ) {
        $api->manager->api_error();
    }

    // Check user can vote in this board
    $token = $api->trello->get_token_info();
    $allowedBoards = $token['boards'] ?? [];
    $userId = $token['userId'] ?? 0;

    if ( empty( $userId ) || empty( $poll->boardId ) || ! in_array( $poll->boardId, $allowedBoards ) ) {
        $api->manager->invalid_auth_error();
    }

    // Get vote
    $vote = $api->get( 'votes', [ 'poll' => $poll->ID, 'user' => $userId ] );
    $vote = $vote[0] ?? [];

    if ( empty( $vote->ID ) ) {
        $vote = (object) array(
            'poll'  => (string) $poll->ID,
            'user'  => (string) $userId,
            'vote'  => '',
        );

        $vote = new Vote( $vote );
    }

    $vote->vote = $new_vote;

    if ( $vote->save() ) {
        $notMyVotes = $api->get( 'votes', [ 'poll' => $poll->ID ] );
        $notMyVotes = array_filter( $notMyVotes, function( $vote ) use ( $userId ) {
            return ( $vote->user != $userId );
        } );

        $api->manager->api_success( array_values( $notMyVotes ) );
    }

    $api->manager->api_error();
}

/**
 * POST
 *
 * ENDPOINT /votes/finish
 * Add a vote (using POST 'poll', 'card', 'vote')
 */

if ( ! empty( $request ) && $request[0] == 'finish' && ! empty( $_POST['id'] ) ) {
    // Sanitize card ID
    $id = filter_var( $_POST['id'], FILTER_SANITIZE_STRING );

    // Get the board of poll
    $poll = $api->get( 'polls', [ 'ID' => $id ] );
    $poll = $poll[0] ?? [];

    if ( empty( $poll->ID ) ) {
        $api->manager->api_error();
    }

    // Check we are voting
    if ( empty( $poll->voting ) ) {
        $api->manager->api_error();
    }

    // Check user can finish this vote
    $token = $api->trello->get_token_info();
    $allowedBoards = $token['boards'] ?? [];
    $userId = $token['userId'] ?? 0;

    if ( empty( $poll->boardId ) || ! in_array( $poll->boardId, $allowedBoards ) ) {
        $api->manager->invalid_auth_error();
    }

    if ( empty( $userId ) || empty( $poll->owner ) || $poll->owner != $userId ) {
        $api->manager->invalid_auth_error();
    }

    // Get vote
    $votes = $api->get( 'votes', [ 'poll' => $poll->ID ] );

    $all_votes = array_map( function( $vote ) {
        return $vote->vote;
    }, $votes );

    // Vote => Frequency
    $all_votes = array_count_values( $all_votes );

    // Big votes first
    krsort( $all_votes );

    // Big frequency first
    arsort( $all_votes );

    $all_votes = array_keys( $all_votes );
    $most_voted = array_shift( $all_votes );

    // Create card name
    $updatecard = [
        'id'    => $poll->voting,
        'name'  => '',
    ];

    if ( ! empty( $most_voted ) ) {
        $updatecard['name'] = $api->trello->create_voted_card_name( $poll->voting, $most_voted );
    }

    // Remove votes
    foreach ( $votes as $vote ) {
        $api->delete( 'votes', $vote );
    }

    // Remove voting
    $poll->voting = false;

    if ( $poll->save() ) {
        $api->manager->api_success( $updatecard );
    }

    $api->manager->api_error();
}

$api->manager->invalid_endpoint_error();