<?php
    global $avant;

    $boardId = ( $avant['original_params'][0] ) ?? '';
    $pollId = ( $avant['original_params'][1] ) ?? '';

    if ( empty( $boardId ) ) av_redirect( 'boards' );

    include_header();
?>

<section id="board" class="page-wrapper" data-bind="component: { name: 'board', params: { id: '<?php echo $boardId; ?>', poll: '<?php echo $pollId; ?>' } }">
    <div class="board-background" data-bind="attr: { style: boardBackground() }"></div>
    <div  class="page-title">
        <h1 class="white-text" data-bind="text: title, visible: title"></h1>
        <i class="material-icons" data-bind="visible: hasPoll">group</i>
        <span data-bind="{ visible: hasPoll, text: pollMembers }"></span>
        <a href="#" class="waves-effect waves-light btn-small blue darken-4" data-bind="{ visible: canCreateOrEnterPoll, click: createOrEnterPoll, css: { 'disabled': isEnteringPoll } }">
            <!-- ko text: enterPollText() --><!-- /ko -->
            <!-- ko if: isEnteringPoll -->
                <!-- ko component: 'btn_loading' --><!-- /ko -->
            <!-- /ko -->
        </a>
        <a href="#" class="waves-effect waves-light btn-small red darken-4" data-bind="{ visible: canDeleteOrExitPoll, click: deleteOrExitPoll, css: { 'disabled': isLeavingPoll } }">
            <!-- ko text: exitPollText() --><!-- /ko -->
            <!-- ko if: isLeavingPoll -->
                <!-- ko component: 'btn_loading' --><!-- /ko -->
            <!-- /ko -->
        </a>
    </div>
    <div class="page-content">
        <div id="lists" class="user-select-none cool-scrollbar" data-bind="foreach: lists">
            <!-- ko component: { name: 'board_list', params: $data } --><!-- /ko -->
        </div>
    </div>
    <!-- ko if: hasPoll -->
        <!-- ko component: { name: 'poll_room' } --><!-- /ko -->
    <!-- /ko -->
</section>

<?php include_footer();
