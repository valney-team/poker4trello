<?php include_header(); ?>

<section id="boards" class="page-wrapper" data-bind='{component: "boards"}'>
    <h1 class="page-title" data-bind="text: title, visible: title"></h1>
    <div id="boards-list" class="row" data-bind="foreach: boards">
        <!-- ko component: { name: 'boards_board', params: $data } --><!-- /ko -->
    </div>
</section>

<?php include_footer();
