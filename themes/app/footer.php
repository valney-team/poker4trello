        <footer class="page-footer blue darken-4">
            <div class="footer-copyright">
                <div class="container center">
                    <?php echo date('Y') . " &copy; " . __('Desenvolvido por Mário Valney usando') ?> <a href="https://projetos.mariovalney.com/avant/">Avant</a> - <?php _e('[BETA]'); ?>
                </div>
            </div>
        </footer>

        <!-- ko component: 'loader' --><!-- /ko -->

        <script type="text/javascript">
            var P4T = {
                APP_NAME: '<?php echo SITE_NAME; ?>',
                BASE_URL: '<?php echo BASE_URL; ?>',
                TRELLO_API_KEY: '<?php echo TRELLO_API_KEY; ?>'
            };
        </script>

        <script type="text/javascript" src="<?php theme_file_url( 'dist/js/jquery.min.js', true, true ) ?>"></script>
        <script type="text/javascript" src="https://trello.com/1/client.js?key=<?php echo TRELLO_API_KEY; ?>"></script>

        <?php $js = ( defined( 'DEBUG' ) && DEBUG ) ? '.js' : '.min.js'; ?>
        <script type="text/javascript" src="<?php theme_file_url( 'dist/js/scripts' . $js, true, true ) ?>"></script>
    </body>
</html>