<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta charset="UTF-8">

        <title><?php av_title(); ?></title>

        <link href="<?php theme_file_url( 'dist/images/favicon.png', true, true ) ?>" rel="icon" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <?php $css = ( defined( 'DEBUG' ) && DEBUG ) ? '.css' : '.min.css'; ?>
        <link rel="stylesheet" type="text/css" href="<?php theme_file_url( 'dist/css/styles' . $css, true, true ); ?>">
    </head>
    <body class="<?php body_class(); ?>">
        <header id="header" class="navbar-fixed" data-bind='{component: "header"}'>
            <nav>
                <div class="nav-wrapper blue darken-4 row">
                    <div class="col s10 offset-s1">
                        <a class="brand-logo center" data-bind="text: title, attr: { href: homeUrl }"></a>
                    </div>
                    <ul class="right">
                        <li class="logout-link" data-bind="visible: isUserLogged()">
                            <a href="#" data-bind="click: logout, clickBubble: false">
                                <?php _e( 'Sair' ); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>