<?php include_header(); ?>

<section id="index" class="page-wrapper page-center-height" data-bind='{component: "index"}'>
    <h1 class="page-title" data-bind="text: title, visible: title"></h1>
    <div id="logged" data-bind="visible: isUserLogged()">
        <p class="flow-text">
            <?php _e( 'Iremos redirecionar para a sua listagem de Boards.' ); ?>
        </p>
    </div>
    <div id="not-logged" data-bind="visible: !isUserLogged()">
        <p class="flow-text">
            <?php _e( 'Antes de começar a trabalhar você precisa autorizar o acesso ao Trello.' ); ?>
        </p>
        <a id="login-to-trello" class="waves-effect waves-light btn" data-bind="click: login, clickBubble: false">
            <?php _e( 'Autorizar' ); ?>
        </a>
    </div>
</section>

<?php include_footer();
