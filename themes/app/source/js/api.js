P4T.api = {
    url: P4T.BASE_URL + 'api/v1/',
    get: function(endpoint, filters, success, error) {
        this.request('GET', endpoint, filters, success, error);
    },
    post: function(endpoint, data, success, error) {
        if (typeof data != 'object') {
            console.log('Atualização de dado inválida');
            return false;
        }

        this.request('POST', endpoint, data, success, error);
    },
    request: function(method, endpoint, params, success, error) {
        ko.postbox.publish('Trello_is_loading', true);

        if (typeof params == 'function') {
            error = success;
            success = params;
            params = {};
        }

        if (typeof params != 'object') {
            params = {};
        }

        if (typeof success != 'function') {
            success = function() {};
        }

        if (typeof error != 'function') {
            error = function() {};
        }

        $.ajax({
            url: P4T.api.url + endpoint,
            type: method,
            data: params,
            headers: {
                'X-P4T-AUTHTOKEN': Trello.token() || '',
            }
        })
        .done(function(response) {
            response = JSON.parse(response);

            if (response.success) {
                success(response);
                return;
            }

            error(response);
        })
        .fail(function(response) {
            error(JSON.parse(response));
        })
        .always(function() {
            ko.postbox.publish('Trello_is_loading', false);
        });
    },
    showError: function() {
        P4T.dialog.showError(
            __( 'Ops... Houve um erro!' ),
            __( 'Tivemos um problema de conexão com o Trello.' ) + '\n' + __( 'Por favor, tente novamente.' )
        );
    }
};