P4T.dialog = {
    error: 'error',
    success: 'success',
    showMessage: function(title, message, type) {
        if (typeof title == 'undefined') {
            title = '';
        }

        if (typeof message == 'undefined') {
            message = '';
        }

        if (typeof type == 'undefined') {
            type = 'neutral';
        }

        message = message.replace(/[\n]/gi, '<br>');

        var data = {
            title: title,
            message: message,
            type: type
        };

        var template = P4T.templates.get('message', function(template) {
            M.toast({ html: P4T.templates.render(template, data) }, 4000);
        });

    },
    showSuccess: function(title, message) {
        this.showMessage(title, message, this.success);
    },
    showError: function(title, message) {
        this.showMessage(title, message, this.error);
    },
};