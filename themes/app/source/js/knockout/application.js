var App = {};

ko.components.loaders.unshift({
    loadTemplate: function(name, templateConfig, callback) {
        if (!templateConfig.loadTemplate) {
            callback(null);
            return;
        }

        P4T.templates.get( templateConfig.loadTemplate, function(template) {
            ko.components.defaultLoader.loadTemplate(name, template, callback);
        } );
    },
});

ko.bindingHandlers.sortable = {
    options: {
        sort: true,
        draggable: '.card-card',
        ghostClass: 'card-moving',
        filter: '.working'
    },
    // This is from: ko.bindingHandlers['foreach'].makeTemplateValueAccessor
    makeForeachValueAccessor: function(valueAccessor) {
        return function() {
            var modelValue = valueAccessor(),
                unwrappedValue = ko.utils.peekObservable(modelValue);

            if ((!unwrappedValue) || typeof unwrappedValue.length == "number") {
                return {
                    'foreach': modelValue,
                    'sortableOptions': {},
                    'templateEngine': ko.nativeTemplateEngine.instance
                };
            }

            ko.utils.unwrapObservable(modelValue);
            return {
                'foreach': unwrappedValue['data'],
                'as': unwrappedValue['as'],
                'includeDestroyed': unwrappedValue['includeDestroyed'],
                'afterAdd': unwrappedValue['afterAdd'],
                'beforeRemove': unwrappedValue['beforeRemove'],
                'afterRender': unwrappedValue['afterRender'],
                'beforeMove': unwrappedValue['beforeMove'],
                'afterMove': unwrappedValue['afterMove'],
                'templateEngine': ko.nativeTemplateEngine.instance,
                'sortableOptions': unwrappedValue['sortableOptions'] || {}
            };
        };
    },
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var options = ko.bindingHandlers.sortable.options,
            foreachValueAccessor = ko.bindingHandlers.sortable.makeForeachValueAccessor(valueAccessor);

        // Parsing Parameters
        ko.utils.objectForEach(foreachValueAccessor().sortableOptions, function(key, value) {
            options[key] = value;
        });

        viewModel.sortable = Sortable.create(element, options);

        // Destroy the sortable if knockout disposes the element
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            viewModel.sortable.destroy();
        });

        return ko.bindingHandlers.foreach.init(element, foreachValueAccessor, allBindings, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        return ko.bindingHandlers.foreach.update(element, valueAccessor, allBindings, viewModel, bindingContext);
    }
};

ko.bindingHandlers.animate = {
    'update': function (element, valueAccessor, allBindings) {
        var value = ko.utils.unwrapObservable( valueAccessor() ),
            animation = allBindings.get('animation');

        if (typeof animation != 'function') {
            animation = function( value, element ) {
                if ( value ) {
                    $(element).show();
                    return;
                }

                $(element).hide();
            }
        }

        animation(value, element);
    }
};

ko.bindingHandlers.fadeVisible = {
    'update': function (element, valueAccessor) {
        var animateBiding = {
            get: function() {
                return function( value, element ) {
                    if ( value ) {
                        $(element).fadeIn();
                        return;
                    }

                    $(element).fadeOut();
                }
            }
        }
        return ko.bindingHandlers.animate.update(element, valueAccessor, animateBiding);
    }
};