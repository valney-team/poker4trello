ko.components.register('board', {
    viewModel: BoardViewModel,
    template: { element: 'board' }
});

function BoardViewModel(param) {
    var view = this;

    view.id = param.id || 0;

    view.title = ko.observable('');
    view.lists = ko.observableArray([]);
    view.boardBackground = ko.observable('');

    view.currentUser = ko.observable(0);

    view.poll = ko.observable({}).publishOn('poll');
    view.inPoll = ko.computed(function() {
        return view.poll().participating || false;
    });
    view.hasPoll = ko.computed(function() {
        var poll = view.poll().ID || false;

        return poll && view.inPoll();
    });

    view.pollMembers = ko.observable(0);
    view.isEnteringPoll = ko.observable(false);
    view.isLeavingPoll = ko.observable(false);

    view.loadingBoard = ko.observable(true);
    view.loadingPoll = ko.observable(true);

    view.isLoading = ko.computed(function() {
        return view.loadingBoard() || view.loadingPoll();
    }, view);

    view.canCreateOrEnterPoll = ko.computed(function() {
        return ! ( view.hasPoll() || view.isLoading() );
    }, view);

    view.canDeleteOrExitPoll = ko.computed(function() {
        return ( view.hasPoll() && ! view.isLoading() );
    }, view);

    view.voting = ko.computed(function() {
        var voting = view.poll().voting || 0,
            lists = view.lists() || [];

        if (!voting || !voting.length) return false;

        for (var i = 0; i < lists.length; i++) {
            var cards = lists[i].cards() || [];

            for (var j = 0; j < cards.length; j++) {
                if (cards[j].id() == voting) {
                    return {
                        id: cards[j].id(),
                        title: cards[j].title(),
                        boardId: cards[j].boardId,
                    };
                }
            }
        }

        return false;
    }, view).publishOn('voting');

    view.isPollOwner = ko.computed(function() {
        var owner = view.poll().owner || -1,
            user = view.currentUser() || 0;

        return ( owner == user );
    }, view);

    view.canStartVote = ko.computed(function() {
        var owner = view.isPollOwner(),
            voting = view.poll().voting || 0;

        return ( ( owner ) && ! voting );
    }, view).publishOn('canStartVote');

    view.canFinishVote = ko.computed(function() {
        var owner = view.isPollOwner(),
            voting = view.poll().voting || 0;

        return ( ( owner ) && voting );
    }, view).publishOn('canFinishVote');

    view.init = function() {
        P4T.trello.isLoggedOrHome(view.renderBoard);
    }

    view.renderBoard = function() {
        if (!view.id) { window.location = '/boards'; }

        P4T.trello.getBoardAndLists(view.id, function(response) {
            var board = response[0]['200'] || false,
                lists = response[1]['200'] || false,
                user = response[2]['200'] || false;

            if (user) {
                view.currentUser(user.id);
            }

            if (board) {
                view.title(board.name);

                var style = '';

                if ( board.prefs && board.prefs.backgroundColor ) {
                    style += 'background-color: ' + board.prefs.backgroundColor + ';';
                }

                if ( board.prefs && board.prefs.backgroundImage ) {
                    style += 'background-image: url(\'' + board.prefs.backgroundImage + '\');';
                }

                view.boardBackground(style);
            } else {
                P4T.dialog.showError(
                    __( 'Ops... Houve um erro!' ),
                    __( 'Você não tem acesso a esse board ou tivemos um problema de conexão com o Trello.' )
                );

                setTimeout(function() {
                    window.location = '/boards';
                }, 3000);
                return;
            }

            if (lists) {
                for (var i = 0; i < lists.length; i++) {
                    lists[i] = {
                        id: ko.observable( lists[i].id ),
                        title: ko.observable( lists[i].name ),
                        cards: ko.observableArray( view.parseCards( lists[i].cards ) ),
                        boardId: view.id
                    };
                }

                view.lists(lists);
            } else {
                P4T.trello.showError();
                return;
            }

            view.loadingBoard(false);
        });

        view.verifyPoll();
    }

    view.createOrEnterPoll = function() {
        if (view.isEnteringPoll()) return;

        var isCreating = true,
            poll = view.poll(),
            data = {
                board: view.id
            };

        if ( poll && poll.ID && ! view.inPoll() ) {
            isCreating = false;
            data.poll = param.poll || poll.ID;
        }

        let action = (isCreating) ? 'create' : 'join';

        view.isEnteringPoll(true);
        P4T.api.post(
            'polls/' + action,
            data,
            function(response) {
                if (!response.success) return;
                if (typeof response.data == 'undefined') return;

                view.poll(response.data);
                view.pollMembers(response.data.users.length);

                if (isCreating) {
                    P4T.dialog.showSuccess(
                        __( 'Sua sala está pronta!' ),
                        __( 'Você já pode compartilhar o link com os colegas e eles irão se juntar a você automaticamente.' )
                    );

                    return;
                }

                P4T.dialog.showSuccess(
                    __( 'Bem-vindo!' ),
                    __( 'Você já pode participar das votações desse board.' )
                );

                view.isEnteringPoll(false);
            }, function(response) {
                if (isCreating) {
                    P4T.dialog.showError(
                        __( 'Não conseguimos criar sua sala.' ),
                        __( 'É possível que uma sala já esteja ativa ou que tenha ocorrido um problema de conexão.' ) + '\n' + __( 'Por favor, tente novamente.' )
                    );

                    return;
                }

                P4T.dialog.showError(
                    __( 'Ops.. Não conseguimos encontrar a sala.' ),
                    __( 'É possível que a sala tenha sido encerrada.' ) + '\n' + __( 'Tente entrar pelo botão ao lado do nome do board.' )
                );

                view.isEnteringPoll(false);
            }
        );
    }

    view.deleteOrExitPoll = function() {
        if (view.isLeavingPoll()) return;

        var isDeleting = view.isPollOwner(),
            poll = view.poll();

        if ( ! poll ) return;

        let action = (isDeleting) ? 'delete' : 'leave';

        view.isLeavingPoll(true);
        P4T.api.post(
            'polls/' + action,
            {id: poll.ID },
            function(response) {
                view.isLeavingPoll(false);

                if (!response.success) return;
                if (typeof response.data == 'undefined') return;

                view.poll({});
                view.pollMembers(0);

                if (isDeleting) {
                    P4T.dialog.showSuccess(
                        __( 'Sua sala foi removida!' ),
                        __( 'Obrigado por utilizar o Poker4Trello.' )
                    );

                    setTimeout(function() { window.location = '/boards'; }, 4000);
                    return;
                }

                P4T.dialog.showSuccess(
                    __( 'Até mais!' ),
                    __( 'Você saiu da sala e não pode mais participar de votações.' )
                );

                setTimeout(function() { window.location = '/boards'; }, 4000);
            }, function(response) {
                view.isLeavingPoll(false);

                if (isDeleting) {
                    P4T.dialog.showError(
                        __( 'Não conseguimos remover sua sala.' ),
                        __( 'É possível que ela já tenha sido removida ou que tenha ocorrido um problema de conexão.' ) + '\n' + __( 'Por favor, tente novamente.' )
                    );

                    return;
                }

                P4T.dialog.showError(
                    __( 'Ops.. Não conseguimos encontrar a sala.' ),
                    __( 'É possível que a sala tenha sido encerrada ou que você já tenha saído.' ) + '\n' + __( 'Por favor, recarregue a página e tente novamente.' )
                );
            }
        );
    }

    view.verifyPoll = function() {
        view.loadingPoll(true);

        P4T.api.get(
            'polls/board/' + view.id,
            function(response) {
                view.loadingPoll(false);

                if (!response.success || !response.data.length) {
                    view.poll({});
                    return;
                }

                view.poll(response.data[0]);
                view.pollMembers(response.data[0].users.length);

                if (!param.poll || response.data[0].participating) return;

                view.createOrEnterPoll();
            }, function(response) {
                view.loadingPoll(false);
            }
        );
    }

    view.enterPollText = function() {
        var poll = view.poll().ID || false;

        if ( poll && ! view.inPoll() ) {
            return __( 'Entrar na sala' );
        }

        return __( 'Criar uma sala' );
    }

    view.exitPollText = function() {
        var poll = view.poll().ID || false;

        if ( poll && view.isPollOwner() ) {
            return __( 'Remover a sala' );
        }

        return __( 'Sair da sala' );
    }

    view.parseCards = function( cards ) {
        if ( ! Array.isArray(cards) ) return [];

        for (var i = 0; i < cards.length; i++) {
            cards[i] = {
                id: ko.observable( cards[i].id ),
                title: ko.observable( cards[i].name ),
                description: ko.observable( cards[i].desc ),
                pos: ko.observable( cards[i].pos ),
                boardId: view.id
            };
        }

        return cards;
    }

    view.poll.subscribe(function(value) {
        var id = value.ID || 0,
            url = '/board/' + view.id,
            participating = value.participating || 0,
            voting = value.voting || 0;

        if (id && participating) {
            url += '/' + id;
        }

        history.pushState({}, document.title, url);
    });

    view.init();
}