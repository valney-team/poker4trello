ko.components.register('board_card', {
    viewModel: BoardCardViewModel,
    template: { loadTemplate: 'card' }
});

function BoardCardViewModel(params) {
    var view = this;

    view.id = params.id;
    view.title = params.title;
    view.description = params.description;
    view.pos = params.pos;
    view.boardId = params.boardId;

    view.formatedDescription = ko.computed(function() {
        var description = view.description() || __('Nenhuma descrição'),
            converter = new showdown.Converter();

        return converter.makeHtml(description);
    }, view);

    view.modalId = function() {
        return 'modal-' + view.id();
    }

    view.cardNameInputId = function() {
        return 'card-name-' + view.id();
    }

    view.openModal = function() {
        var template = $('#' + view.modalId());
        if (!template.length) return;

        var modal = template.clone().appendTo('body');
        modal.modal({
            onCloseEnd: function() {
                $(modal).remove();
            }
        }).modal('open');
    }

    view.startVoteText = function() {
        return __('Votar');
    }

    ko.postbox.subscribe('card-' + view.id(), function(card) {
        if (typeof card.name !== 'undefined') view.title(card.name);
        if (typeof card.desc !== 'undefined') view.description(card.desc);
        if (typeof card.pos !== 'undefined') view.pos(card.pos);
    });
}