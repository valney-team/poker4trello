ko.components.register('board_list', {
    viewModel: BoardListViewModel,
    template: { loadTemplate: 'list' }
});

function BoardListViewModel(params) {
    var view = this;

    view.id = params.id;
    view.title = params.title;
    view.cards = params.cards;
    view.boardId = params.boardId;

    view.listId = function() {
        return 'list-' + view.id();
    }

    view.sortableOptions = function() {
        return {
            group: view.boardId,
            onEnd: onEnd
        };
    }

    function calculateNewPost(previousElement, nextElement) {
        var prevPos = false, nextPos = false;

        if (previousElement) {
            prevPos = ko.dataFor(previousElement).pos() || false;
        }

        if (nextElement) {
            nextPos = ko.dataFor(nextElement).pos() || false;
        }

        if (prevPos && nextPos) {
            return ( ( nextPos - prevPos ) / 2 ) + prevPos;
        }

        if (prevPos) {
            return 'bottom';
        }

        return 'top';
    }

    function onEnd(event) {
        if (event.newIndex === event.oldIndex && event.from === event.to) return;

        var card = ko.dataFor(event.item),
            oldList = ko.dataFor(event.from),
            newList = ko.dataFor(event.to),
            previousElement = event.item.previousElementSibling || false,
            nextElement = event.item.nextElementSibling || false,
            newPos = calculateNewPost(previousElement, nextElement),
            success = function(response) {
                var oldListCards = oldList.cards(),
                    newListCards = newList.cards(),
                    newCardsToOldList = [],
                    newCardsToNewList = [];

                // Update Card Info
                card.title(response.name);
                card.pos(response.pos);

                // It's the same list
                if (oldList.id() == newList.id()) return;

                // Update Old List
                for (var i = 0; i < oldListCards.length; i++) {
                    if (oldListCards[i].id() == response.id) continue;
                    newCardsToOldList.push(oldListCards[i]);
                }

                // Update New List
                for (var i = 0; i < newListCards.length; i++) {
                    if (i == event.newIndex) {
                        newCardsToNewList.push({
                            id: card.id,
                            title: card.title,
                            pos: card.pos,
                            boardId: card.boardId
                        });
                    }

                    newCardsToNewList.push(newListCards[i]);
                }

                // Clean lists
                oldList.cards([]);
                newList.cards([]);

                // Add new data
                oldList.cards(newCardsToOldList);
                newList.cards(newCardsToNewList);
            },
            error = function() {
                var oldListCards = oldList.cards(),
                    newListCards = newList.cards();

                // Clean lists
                oldList.cards([]);
                newList.cards([]);

                // Add new data
                oldList.cards(oldListCards);
                newList.cards(newListCards);
            };

        P4T.trello.moveCardToListWithPosition( card.id(), newList.id(), newPos, success, error );
    }
}