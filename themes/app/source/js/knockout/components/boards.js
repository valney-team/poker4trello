ko.components.register('boards', {
    viewModel: BoardsViewModel,
    template: { element: 'boards' }
});

function BoardsViewModel() {
    var view = this;

    view.title = __( 'Escolha seu projeto' );

    view.boards = ko.observableArray([]);

    view.init = function() {
        P4T.trello.isLoggedOrHome(view.listBoards);
    }

    view.listBoards = function() {
        P4T.trello.getBoards(function(response) {
            var boardIds = [];

            for (var i = 0; i < response.length; i++) {
                boardIds.push(response[i].shortLink);

                response[i] = {
                    id: ko.observable( response[i].id ),
                    title: ko.observable( response[i].name ),
                    attrTitle: ko.observable( ( response[i].desc || __( 'Board:' ) + ' ' + response[i].name ) ),
                    backgroundColor: ko.observable( ( response[i].prefs.backgroundColor || '#B04632' ) ),
                    backgroundImage: ko.observable( ( response[i].prefs.backgroundImage || '' ) ),
                    shortLink: ko.observable( response[i].shortLink ),
                    hasPoll: ko.observable( false )
                };
            }

            view.boards(response);

            if (boardIds.length) {
                P4T.api.get(
                    'polls', { 'boards': boardIds.join(',') },
                    function(response) {
                        if (!response.success) return;

                        var boards = view.boards();
                        for (var i = 0; i < response.data.length; i++) {
                            for (var j = 0; j < boards.length; j++) {
                                if (response.data[i].boardId != boards[j].shortLink()) continue;
                                boards[j].hasPoll(true);
                            }
                        }
                    }
                );
            }
        });
    }

    view.init();
}