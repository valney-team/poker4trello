ko.components.register('boards_board', {
    viewModel: BoardsBoardViewModel,
    template: { loadTemplate: 'board' }
});

function BoardsBoardViewModel(params) {
    var view = this;

    view.id = params.id;
    view.title = params.title;
    view.attrTitle = params.attrTitle;
    view.backgroundColor = params.backgroundColor;
    view.backgroundImage = params.backgroundImage;
    view.shortLink = params.shortLink;
    view.hasPoll = params.hasPoll;

    view.boardBackground = function() {
        var style = '';

        if ( view.backgroundColor() ) {
            style += 'background-color: ' + view.backgroundColor() + ';';
        }

        if ( view.backgroundImage() ) {
            style += 'background-image: url(\'' + view.backgroundImage() + '\');';
        }

        return style;
    }

    view.boardLink = function() {
        return '/board/' + view.shortLink();
    }
}