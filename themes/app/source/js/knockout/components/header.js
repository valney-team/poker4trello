ko.components.register('header', {
    viewModel: HeaderViewModel,
    template: { element: 'header' }
});

function HeaderViewModel() {
    var view = this;

    view.title = P4T.APP_NAME;
    view.homeUrl = ko.pureComputed(() => {
        var url = P4T.BASE_URL;
        if ( view.isUserLogged() ) {
            url += 'boards';
        }

        return url;
    });

    view.isUserLogged = ko.observable(false).syncWith('isUserLogged');

    view.logout = function(data, event) {
        P4T.trello.logout();
        view.isUserLogged(false);
    }
}