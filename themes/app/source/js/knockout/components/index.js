ko.components.register('index', {
    viewModel: IndexViewModel,
    template: { element: 'index' }
});

function IndexViewModel() {
    var view = this;

    view.title = __( 'Bem-vindo' );
    view.isUserLogged = ko.observable(false).syncWith('isUserLogged');

    view.init = function() {
        P4T.trello.isLogged(function() {
            view.isUserLogged(true);
        }, function() {
            view.isUserLogged(false);
        });
    }

    view.login = function() {
        P4T.trello.login(function() {
            view.isUserLogged(true);
        }, function() {
            view.isUserLogged(false);
        });
    }

    view.isUserLogged.subscribe(function(value) {
        if (value) {
            window.location = '/boards';
        }
    });

    view.init();
}