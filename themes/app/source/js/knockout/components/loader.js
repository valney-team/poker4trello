ko.components.register('loader', {
    viewModel: LoaderViewModel,
    template: { loadTemplate: 'loader' }
});

function LoaderViewModel() {
    var view = this;

    view.isLoading = ko.observable(false).subscribeTo('Trello_is_loading');
}