ko.components.register('poll_room', {
    viewModel: PollRoomViewModel,
    template: { loadTemplate: 'poll-room' }
});

function PollRoomViewModel(params) {
    var view = this;

    view.refreshingVoteInterval = 0;
    view.refreshingVotes = false;

    view.poll = ko.observable({}).subscribeTo('poll', true, function(poll) {
        var the_poll = poll.ID || 0;

        if (!the_poll) return poll;

        P4T.api.get(
            'votes/poll',
            { id: the_poll },
            function(response) {
                if (!response.success) return;

                var votes = response.data || [],
                    yours = {};

                if ( typeof votes.user != 'undefined' ) {
                    yours = {
                        user: __( 'Você' ),
                        vote: votes.user || '?'
                    };

                    view.currentVote(votes.user);

                    votes = [yours].concat(votes.others);
                    view.votes(votes);
                }
            },
            function(response) {
                P4T.dialog.showError(
                    __( 'Ops... Não conseguimos recuperar os votos.' ),
                    __( 'Talvez esse card tenha sido excluído no Trello ou a votação foi encerrada.' ) + '\n' + __( 'Recarregue a página e tente novamente.' )
                );
            }
        );

        return poll;
    });

    view.pollIsOpen = ko.observable(false);

    view.users = ko.observableArray([]);

    view.currentVote = ko.observable('?');

    view.votes = ko.observableArray([]);

    view.voting = ko.observable().subscribeTo('voting', true, function(card) {
        if (typeof card.forceOpen != 'undefined' && card.forceOpen) {
            delete card.forceOpen;
            view.pollIsOpen(true);
        }

        return card;
    });

    view.votingText = ko.pureComputed(function() {
        var title = __('Nenhum card sendo votado');

        if ( ( view.voting().title || false ) ) {
            title = __('Votando:') + ' ' + view.voting().title;
        }

        return title;
    }, view);

    view.translate = function(string) {
        return __(string);
    }

    view.toogleText = function() {
        return ( view.pollIsOpen() ) ? __( 'Fechar' ) : __( 'Abrir' );
    }

    view.tooglePoll = function() {
        view.pollIsOpen( ! view.pollIsOpen() );
    }

    view.slidePoll = function(isVisible, element) {
        let maxWidth = $(window).width();

        if (isVisible) {
            $(element).animate({
                'max-width': maxWidth,
                'opacity': 1
            }, 600);

            return;
        }

        $(element).animate({
                'max-width': 0,
                'opacity': 0.5,
            }, 400,
            function() {
                $(element).css('opacity', 0);
            }
        );
    }

    view.pokerOptions = function() {
        return [ '0', '1/2', '1', '2', '3', '5', '8', '13', '20', '40', '?' ];
    }

    // Stop voting
    view.finishVoting = function() {
        var poll = view.poll() || [];
        if (typeof poll.ID == 'undefined') return;

        P4T.api.post(
            'votes/finish',
            { id: poll.ID },
            function(response) {
                if (!response.success) return;
                if (!response.data) return;

                view.votes([]);
                view.currentVote('?');
                view.pollIsOpen(false);
                ko.postbox.publish('voting', false);
                ko.postbox.publish('canStartVote', true);
                ko.postbox.publish('canFinishVote', false);

                if (typeof response.data.id == 'undefined' || !response.data.id) return;
                if (typeof response.data.name == 'undefined' || !response.data.name) return;

                P4T.trello.changeCardName(response.data.name, response.data.id, function() {
                    P4T.dialog.showSuccess(
                        __( 'Votação encerrada' ),
                        __( 'Os votos foram contados e já salvamos no Trello.' )
                    );
                });
            },
            function() {
                P4T.dialog.showError(
                    __( 'Ops... Não conseguimos finalizar a votação.' ),
                    __( 'Talvez esse card tenha sido excluído no Trello ou a votação já tenha sido encerrada.' ) + '\n' + __( 'Recarregue a página e tente novamente.' )
                );
            }
        );
    }

    // Update user vote
    view.updateVote = function(obj, event) {
        if (!event.originalEvent) return;

        var vote = event.originalEvent.target.value,
            validVotes = view.pokerOptions(),
            poll = view.poll().ID || 0,
            card = view.voting().id || 0;

        if (! poll || ! card || $.inArray(vote, validVotes) < 0) {
            P4T.dialog.showError( '', __( 'Ops... Voto inválido.' ) );
            return false;
        }

        P4T.api.post(
            'votes/add',
            {
                poll: poll,
                card: card,
                vote: vote
            },
            function(response) {
                if (!response.success) return;

                var votes = response.data || [],
                    yours = {
                        user: __( 'Você' ),
                        vote: vote
                    };

                votes = [yours].concat(votes);
                view.votes(votes);
            },
            function() {
                P4T.dialog.showError(
                    __( 'Ops... Não conseguimos salvar seu voto.' ),
                    __( 'Talvez esse card tenha sido excluído no Trello ou a votação foi encerrada.' ) + '\n' + __( 'Recarregue a página e tente novamente.' )
                );
            }
        );
    };

    view.refreshVotes = function() {
        var the_poll = view.poll.ID || 0;

        if (!the_poll) return;
        if (!view.voting()) return;
        if (view.refreshingVotes) return;

        view.refreshingVotes = true;

        P4T.api.get(
            'votes/poll',
            { id: the_poll },
            function(response) {
                if (!response.success) return;

                var votes = response.data || [],
                    yours = {};

                if ( typeof votes.user != 'undefined' ) {
                    yours = {
                        user: __( 'Você' ),
                        vote: votes.user || '?'
                    };

                    view.currentVote(votes.user);

                    votes = [yours].concat(votes.others);
                    view.votes(votes);
                }

                view.refreshingVotes = false;
            },
            function(response) {
                P4T.dialog.showError('', __( 'Você está desconectado?' ));
                view.refreshingVotes = false;
            }
        );
    }

    view.pollIsOpen.subscribe(function(isOpen) {
        clearInterval(view.refreshingVoteInterval);

        if ( ! isOpen ) return;
        view.refreshingVoteInterval = setInterval(() => {
            view.refreshVotes();
        }, 3000);
    });
}