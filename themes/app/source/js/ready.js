jQuery(document).ready(function($) {
    if (typeof Trello !== 'object') {
        P4T.dialog.sendError(
            __( 'Ops... Houve um erro!' ),
            __( 'Parece que não conseguimos carregar a API do Trello.' ) + '\n' + __( 'Tente novamente.' )
        );

        return;
    }

    // Card Name edition (inside modal)
    $('body').on('change', '.editable-card-name', function(event) {
        event.preventDefault();

        var card = $(this).data('card') || false,
            name = $(this).val();

        if (!card) return false;

        P4T.trello.changeCardName(name, card);
    });

    // Start vote (inside modal)
    $('body').on('click', '.start-vote-trigger', function(event) {
        event.preventDefault();

        var card = $(this).data('card') || 0;

        if (!card) return false;

        // Empty current voting
        ko.postbox.publish('voting', false);

        P4T.api.post(
            'votes/card',
            {id: card},
            function(response) {
                if (!response.success) return;

                ko.postbox.publish('canStartVote', false);
                ko.postbox.publish('canFinishVote', true);

                ko.postbox.publish('voting', {
                    id: response.data.id || 0,
                    title: response.data.name || '',
                    boardId: response.data.board.id || 0,
                    forceOpen: true,
                });
            },
            function() {
                P4T.dialog.showError(
                    __( 'Ops... Não conseguimos iniciar a votação.' ),
                    __( 'Talvez esse card tenha sido excluído no Trello ou outra votação esteja em andamento.' ) + '\n' + __( 'Recarregue a página e tente novamente.' )
                );
            }
        );
    });

    ko.postbox.subscribe('canStartVote', function(newValue) {
        newValue = ( newValue ) ? true : false;
        $('body').toggleClass('can-start-vote', newValue);
    }, null, true);

    ko.postbox.subscribe('canFinishVote', function(newValue) {
        newValue = ( newValue ) ? true : false;
        $('body').toggleClass('can-finish-vote', newValue);
    }, null, true);
});