P4T.templates = {
    templates: {},
    checkForConditionals: function(template, values) {
        var regex = null,
            regexResult = null,
            value = null,
            operator = null,
            conditionalShouldRemove = function(operator, values, value) {
                if (operator == '=' && (!values.hasOwnProperty(value) || !values[value])) { return true; }
                if (operator == '!=' && (values.hasOwnProperty(value) && values[value])) { return true; }

                return false;
            };

        do {
            regex = new RegExp('{{if(=|!=){1}(\\w*|\\d*){1}}{1}(?:\\n|\\s|.(?!(?:{if|{endif)))*{{endif}}{1}', 'gmi'),
            regexResult = regex.exec(template);

            if (regexResult && regexResult.length > 0) {
                operator = regexResult[1];
                value = regexResult[2];

                template = template.replace(regexResult[0], function(matchToReplace) {
                    if (conditionalShouldRemove(operator, values, value)) {
                        return '';
                    }

                    matchToReplace = matchToReplace.replace('{{if' + operator + value + '}}', '');
                    matchToReplace = matchToReplace.replace('{{endif}}', '');
                    return matchToReplace;
                });
            }
        }
        while (regexResult && regexResult.length > 0);

        return template;
    },
    get: function(name, success) {
        if (typeof success != 'function') return;

        if (typeof this.templates[name] != 'undefined') {
            success(this.templates[name]);
            return;
        }

        jQuery.ajax({
            url: '/templates/' + name
        })
        .done(function(response) {
            P4T.templates.templates[name] = response;
            success(response);
        });
    },
    render: function(template, values) {
        for (var key in values){
            if (values.hasOwnProperty(key)) {
                var regex = new RegExp('{{' + key + '}}', 'gi');
                template = template.replace(regex, values[key]);
            }
        }

        template = P4T.templates.checkForConditionals(template, values);

        return template;
    },
};