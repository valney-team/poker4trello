P4T.trello = {
    isLogged: function(success, error) {
        console.log('Checando o Usuário');

        Trello.authorize({
            interactive: false,
            success: success,
            error: error
        });
    },
    isLoggedOrHome: function(success) {
        console.log('Área Logada');

        this.isLogged(function(response) {
            ko.postbox.publish('isUserLogged', true);
            return success(response);
        }, function() {
            window.location = '/';
        });

        ko.postbox.subscribe('isUserLogged', function(value) {
            if (!value) window.location = '/';
        });
    },
    login: function(success, error) {
        console.log('Autorizando o Trello');

        Trello.authorize({
            type: 'popup',
            name: P4T.APP_NAME,
            scope: {
                read: true,
                write: true
            },
            expiration: '1day',
            success: success,
            error: error
        });
    },
    logout: function() {
        console.log('Desautorizando o Trello');

        Trello.deauthorize();
    },
    getBoards: function(success) {
        Trello.get(
            'member/me/boards',
            { filter: 'open' },
            success, this.showError
        );
    },
    getBoardAndLists: function(id, success) {
        var urls = [
            '/boards/' + id + '?filter=open&members=all',
            '/boards/' + id + '/lists?filter=open&cards=open',
            '/members/me'
        ];

        Trello.get(
            'batch',
            { urls: urls.join(',') },
            success, this.showError
        );
    },
    moveCardToListWithPosition: function(cardId, listId, newPos, success, error) {
        Trello.put(
            'cards/' + cardId,
            { idList: listId, pos: newPos },
            success, error
        );
    },
    changeCardName: function(name, cardId) {
        Trello.put(
            'cards/' + cardId,
            { name: name },
            function(response) {
                ko.postbox.publish('card-' + response.id, response);
            }, this.showError
        );
    },
    showError: function(response) {
        /**
         * Check if we have a invalid token
         * Trello.authorize with interactive: false still send success callback
         * if user revoked its token. We should check and logout properly.
         */
        if ( response.responseText === 'invalid token' && window.location.pathname !== '/' ) {
            Trello.deauthorize();
            window.location = '/';
            return;
        }

        // If is a common error, show.
        P4T.dialog.showError(
            __( 'Ops... Houve um erro!' ),
            __( 'Tivemos um problema de conexão com o Trello.' ) + '\n' + __( 'Por favor, tente novamente.' )
        );
    }
};

// Intercept Trello client to add Loading information

var oldRest = Trello.rest;
Trello.rest = function() {
    return function() {
        ko.postbox.publish('Trello_is_loading', true);

        var oldSuccess = arguments[3] || function(){};
        arguments[3] = function() {
            ko.postbox.publish('Trello_is_loading', false);
            oldSuccess.apply(null, arguments);
        }

        var oldError = arguments[4] || function(){};
        arguments[4] = function() {
            ko.postbox.publish('Trello_is_loading', false);
            oldError.apply(null, arguments);
        }

        return oldRest.apply(null, arguments);
    }
}