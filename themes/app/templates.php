<?php

global $avant;

if ( empty( $avant['query_params'] ) || empty( $avant['query_params'][0] ) ) exit;

$file = ROOT . THEMES_DIR . DS . THEME . DS . 'templates' . DS . $avant['query_params'][0] . '.html';

if ( ! is_readable( $file ) ) exit;

include $file;